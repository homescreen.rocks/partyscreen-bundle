FROM johnpapa/angular-cli AS frontend

RUN apk add --no-cache git
RUN mkdir -p /src

WORKDIR /src
RUN git clone https://gitlab.com/homescreen.rocks/partyscreen-ui.git
WORKDIR /src/partyscreen-ui
RUN npm install
RUN ng build --prod --base-href /beamer/

WORKDIR /src
RUN git clone https://gitlab.com/homescreen.rocks/partyscreen-upload.git
WORKDIR /src/partyscreen-upload
RUN npm install
RUN ng build --prod

FROM registry.gitlab.com/homescreen.rocks/partyscreen-backend/master:latest AS backend

FROM alpine:3.7

RUN apk add ca-certificates --no-cache

COPY --from=frontend /src/partyscreen-upload/dist/ /src/
COPY --from=frontend /src/partyscreen-ui/dist/ /src/beamer
COPY --from=backend /usr/local/bin/partyscreen-backend /usr/local/bin/partyscreen-backend

COPY run.sh /run.sh
RUN chmod +x /run.sh

ENTRYPOINT /run.sh
